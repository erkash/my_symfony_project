$(document).ready(function () {
    $(".follow").on('click', function () {
        var followers = $('.followers');
        var
            $this = $(this),
            action = $this.data('action'),
            id = $this.data('id')
        ;

        $.post(
            Routing.generate('app_users_' + action, {id: id})
        )
            .done(function (response) {
                if (action === 'subscribe') {
                    $("#subscribe a").removeClass('btn-primary');
                    $("#subscribe a").addClass('btn-warning');
                    $this.find('#subscribe-action').html('Unsubscribe');
                    $this.data('action', 'unsubscribe');
                    followers = $('<li></li>').append(response.follower);
                } else {
                    $("#subscribe a").removeClass('btn-warning');
                    $("#subscribe a").addClass('btn-primary');
                    $this.data('action', 'subscribe');
                    $this.find("#subscribe-action").html('Subscribe');
                    followers = $('<li></li>').remove(response.follower);
                }
            })
            .fail(function () {
                alert('Oops, Something went wrong!');
            });
        return false;
    });
});