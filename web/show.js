$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
});

$(document).ready(function () {
    $("form").submit(function () {
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize()
        })
            .done(function () {
                $("form").trigger("reset");
            })
            .fail(function () {
                alert('Oops, Something went wrong!');
            });
        return false;
    });


    $(".custom").on('click', function () {
        var likesCount = $('.likes_count');

        var
            $this = $(this),
            action = $this.data('action'),
            id = $this.data('id')
        ;

        $.post(
            Routing.generate('app_users_' + action, {id: id})
        )
            .done(function (response) {
                likesCount.html(response.likesCount);
                if (action === 'like') {
                    $("#like-unlike a").removeClass('btn-success');
                    $("#like-unlike a").addClass('btn-danger');
                    $this.find('#like-action').html('Unlike');
                    $this.data('action', 'unlike');
                } else {
                    $("#like-unlike a").removeClass('btn-danger');
                    $("#like-unlike a").addClass('btn-success');
                    $this.data('action', 'like');
                    $this.find("#like-action").html('Like')
                }
            })
            .fail(function () {
                alert('Oops, Something went wrong!');
            });
        return false;
    });
});

$(function () {
    var show = 3;
    var $comments = $("#commentsBlock>.post-comment");

    var $update = function () {
        var vis = 0;
        var pos = 0;
        $comments.each(function () {
            pos++;
            $(this).css({"display": (pos > show) ? 'none' : 'block'});

            if (pos <= show) {
                vis++;
            }
        });

        if (vis === $comments.length || $comments.length === 0) {
            console.log($comments.length);
            $("#btnMore").css({"display": "none"});
        }
    };

    $update();

    var btnMore = $("<div />").attr({"id": "btnMore"});
    btnMore.html("Show more");
    btnMore.click(function () {
        show = show + 3;
        $update();
    });

    $(".comment-container").append(btnMore);

});