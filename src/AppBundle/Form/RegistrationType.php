<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    const LABELS = ['POST' => 'register', 'PUT' => 'Update', 'DELETE' => 'Delete'];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add($options['method'], SubmitType::class, [
                'label' => self::LABELS[$options['method']],
                'attr' => [
                    'class' => 'btn btn-primary btn-md'
            ]])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}