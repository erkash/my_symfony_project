<?php

namespace AppBundle\Form;

use FOS\RestBundle\View\View;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class FormErrorsSerializerTrait
 *
 * @package AppBundle\Form
 * @author  Erkin Azimbaev <erkin.azimbaev@sibers.com>
 */
trait FormErrorsTrait
{
    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return View
     */
    protected function getErrorsView(ConstraintViolationList $errors)
    {
        $messages = [];
        $errorIterator = $errors->getIterator();
        foreach ($errorIterator as $validationError) {
            $messages[$validationError->getPropertyPath()][] = $validationError->getMessage();
        }
        $view = View::create($messages);
        $view->setStatusCode(400);

        return $view;
    }
}
