<?php

namespace AppBundle\Service;


use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class Mailer
{
    private $mailer;
    private $engine;
    private $params;
    private $message;

    const TEMPLATE = '@App/Email/new-post.html.twig';

    public function __construct(\Swift_Mailer $mailer, EngineInterface $engine, $params)
    {
        $this->mailer = $mailer;
        $this->engine = $engine;
        $this->params = $params;
    }

    public function create(Article $article, User $author)
    {
        $deliveryAddresses = [];

        /** @var User $follower */
        foreach ($author->getFollowers() as $follower) {
            $deliveryAddresses[] = $follower->getEmail();
        }

        $body = $this->engine->render(self::TEMPLATE, [
            'article' => $article,
            'author' => $author
        ]);


        $this->message = \Swift_Message::newInstance()
            ->setSubject($this->params['subject'])
            ->setFrom($this->params['from'])
            ->setTo($deliveryAddresses)
            ->setBody($body, 'text/html');

        return $this;
    }


    public function send()
    {
        return $this->mailer->send($this->message);
    }

}