<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use AppBundle\Form\ArticleType;
use AppBundle\Form\FormErrorsTrait;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RestArticleController extends FOSRestController
{
    use FormErrorsTrait;

    /**
     * @Annotations\View()
     * @param Request $request
     * @return Article[]
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a collection of Article",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the article is not found"
     *   },
     *     output={"collection"=true, "collectionName"="articles", "class"="AppBundle\Entity\Article"}
     * )
     */
    public function getArticlesAction(Request $request)
    {
        return $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getRepository('AppBundle:Article')->findAll(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            $request->query->getInt('limit', 10)/*limit per page*/
        );
    }

    /**
     * @param Article $article
     * @Annotations\View()
     * @return Article
     * @ApiDoc(
     *   resource = true,
     *   description = "Return an article identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the article is not found"
     *   },
     *      parameters={
     *      {"name"="article", "dataType"="integer", "required"=true, "description"="article id"}
     *  },
     *     output={"collection"=true, "collectionName"="articles", "class"="AppBundle\Entity\Article"}
     * )
     */
    public function getArticleAction(Article $article)
    {
        return $article;
    }

    /**
     * @param Article $article
     * @return array
     * @Annotations\View(serializerGroups={"article", "comment"})
     * @ApiDoc(
     *   resource = true,
     *   description = "Return all comments of an article identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   },
     *      parameters={
     *      {"name"="article", "dataType"="integer", "required"=true, "description"="article id"}
     *  },
     *     output={"collection"=true, "collectionName"="comments", "class"="AppBundle\Entity\Comment"}
     * )
     */
    public function getArticleCommentsAction(Article $article)
    {
        return $article->getComments()->toArray();
    }

    /**
     * @param Request $request
     * @return Response|view
     * @ApiDoc(
     *     description = "Creates new article",
     *     input="AppBundle\Form\ArticleType",
     *     output="AppBundle\Entity\Article",
     *     statusCodes = {
     *          201 = "Returned when successful",
     *          404 = "Returned when not found"
     *   }
     *  )
     */
    public function postArticleAction(Request $request)
    {
        $form = $this->createForm(ArticleType::class, null, [
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);
        /** @var Article $article */
        $article = $form->getData();

        $errors = $this->get('validator')->validate($article, null, ['Article_create']);

        if ($errors->count() == 0) {
            $article->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $view = $this->view($article, 201);
            return $this->handleView($view);
        } else {
            $view = $this->getErrorsView($errors);
            return $view;
        }
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return Response|view
     *
     * @ApiDoc(
     *     input="AppBundle\Form\ArticleType",
     *     output="AppBundle\Entity\Article",
     *     description = "Edit an article identified by id",
     *     statusCodes={
     *         204 = "Returned when an existing Article has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putArticleAction(Request $request, Article $article)
    {
        if (!$article) {
            throw new NotFoundHttpException('Article not found');
        }

        $form = $this->createForm(ArticleType::class, $article, [
            'csrf_protection' => false,
        ]);

        $form->handleRequest($request);
        /** @var Article $article */
        $article = $form->getData();

        $errors = $this->get('validator')->validate($article, null, ['Article_create']);

        if ($errors->count() == 0) {
            $article->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $view = $this->view($article, 200);
            return $this->handleView($view);
        } else {
            $view = $this->getErrorsView($errors);
            return $view;
        }
    }


    /**
     * Delete an article identified by id.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete an article identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the article is not found"
     *   }
     * )
     *
     * @param Article $article
     * @return View
     */
    public function deleteArticleAction(Article $article)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var User $author */
        $author = $article->getUser();

        if ($user->getId() == $author->getId()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();

            $view = View::create();
            $view->setStatusCode(204);

            return $view;
        } else {
            throw new AccessDeniedHttpException('Access denied');
        }
    }

}