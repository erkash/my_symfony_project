<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TagsController extends Controller
{
    /**
     * @Route("/showTag/{name}", name="show_by_tag")
     * @param Request $request
     * @param Tag $tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showTagAction(Request $request, Tag $tag)
    {
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $paginateArticle = $paginator->paginate(
            $tag->getArticles(),
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('AppBundle:Tags:show_tag.html.twig', ['articlesTags' => $paginateArticle]);
    }
}
