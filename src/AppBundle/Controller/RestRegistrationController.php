<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\FormErrorsTrait;
use AppBundle\Form\RegistrationType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\View\View;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RouteResource("rest", pluralize=false)
 */
class RestRegistrationController extends FOSRestController
{
    use FormErrorsTrait;

    /**
     * Create a User from the submitted data.<br/>
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new user from the submitted data.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   },
     *     input="AppBundle\Form\RegistrationType",
     * )
     *
     * @param Request $request
     * @return View|Response
     */
    public function postRegisterAction(Request $request)
    {
        $form = $this->createForm(RegistrationType::class, null, [
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        $userManager = $this->container->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->createUser();
        $user->setUsername($request->request->get('app_user_registration')['username']);
        $user->setEmail($request->request->get('app_user_registration')['email']);
        $user->setPlainPassword($request->request->get('app_user_registration')['plainPassword']['first']);
        $user->setPlainPassword($request->request->get('app_user_registration')['plainPassword']['second']);
        $user->setName($request->request->get('app_user_registration')['name']);
        $user->setSurname($request->request->get('app_user_registration')['surname']);
        $user->setEnabled(true);
        $user->addRole('ROLE_API');

        $errors = $this->get('validator')->validate($user, null, ['Registration']);

        if ($errors->count() == 0) {
            $userManager->updateUser($user);
            /** @var JWTManager $jwtManager */
            $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
            /** @var EventDispatcher $dispatcher */
            $dispatcher = $this->get('event_dispatcher');

            $jwt = $jwtManager->create($user);
            $response = new JsonResponse();
            $event = new AuthenticationSuccessEvent(array('token' => $jwt), $user, $response);
            $event->getResponse();
            $dispatcher->dispatch(Events::AUTHENTICATION_SUCCESS, $event);
            $response->setData($event->getData());

            return $response;
        } else {
            $view = $this->getErrorsView($errors);
            return $view;
        }
    }
}
