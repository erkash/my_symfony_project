<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use AppBundle\Form\AvatarType;
use AppBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{
    /**
     * @Route("/user/{slug}", name="profile")
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(Request $request, User $user)
    {
        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $contains = $user->getFollowers()->contains($this->getUser());
        $followers = $user->getFollowers();

        $form = $this->createForm(AvatarType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $user->getImageFile();
            $user->setAvatar($file);
            $user->setAvatarIsChanged(true);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('profile', ['slug' => $user->getSlug()]);
        }

        return $this->render('@App/Users/profile.html.twig', [
            'form' => $form->createView(),
            'profile' => $user,
            'contains' => $contains,
            'followers' => $followers
        ]);
    }

    /**
     * @Route("/user/{slug}/edit", name="edit_profile")
     * @param Request $request
     * @param User $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfileAction(Request $request, User $profile)
    {
        $form = $this->createForm(RegistrationType::class, $profile, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('profile', ['slug' => $profile->getSlug()]);
        }

        return $this->render('@App/Users/edit_profile.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/like/{id}", options={"expose"=true})
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likeAction(Article $article)
    {
        /** @var User $user */
        $user = $this->getUser();

        $article->addLikeUser($user);
        $user->addLikeArticle($article);

        $em = $this->getDoctrine()->getManager();

        $em->persist($article);
        $em->persist($user);
        $em->flush();

        return new JsonResponse(['likesCount' => $article->getLikeUsers()->count()]);
    }

    /**
     * @Route("/unlike/{id}", options={"expose"=true})
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function unLikeAction(Article $article)
    {
        /** @var User $user */
        $user = $this->getUser();
        $article->removeLikeUser($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        return new JsonResponse(['likesCount' => $article->getLikeUsers()->count()]);
    }

    /**
     * @Route("/subscribe/{id}", options={"expose"=true})
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function subscribeAction(User $user)
    {
        /** @var User $follower */
        $follower = $this->getUser();
        if (!$follower) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $user->addFollower($follower);
        $follower->addFollowing($user);

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->persist($follower);
        $em->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/unsubscribe/{id}", options={"expose"=true})
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function unSubscribeAction(User $user)
    {
        /** @var User $follower */
        $follower = $this->getUser();
        if (!$follower) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $user->removeFollower($follower);
        $follower->removeFollowing($user);

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->persist($follower);
        $em->flush();

        return new JsonResponse();
    }
}
