<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CommentsController extends Controller
{
    /**
     * @Route("/article-{id}/new-comment", name="comment_new")
     * @Method("POST")
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_USER')")
     * @throws \Exception
     */
    public function newAction(Request $request, Article $article)
    {
        if ($article->getCommentsIsDisabled()) {
            throw new \Exception('comments are disabled');
        }

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment->setUser($this->getUser());
            $comment->setArticle($article);

            $em->persist($comment);
            $em->flush();

            $content = $this->renderView('@App/Comment/template.html.twig', [
                'comment' => $comment
            ]);

            return new JsonResponse([
                'content' => $content,
                'comment_count' => $article->getComments()->count()
            ]);
        }

        return new JsonResponse([], 500);
    }
}
