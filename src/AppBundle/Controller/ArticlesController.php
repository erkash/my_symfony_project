<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Article;
use AppBundle\Entity\Tag;
use AppBundle\Entity\User;
use AppBundle\Entity\ViewCounter;
use AppBundle\Event\MyCustomEvent;
use AppBundle\Form\ArticleType;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ArticlesController extends Controller
{
    /**
     * @Route("/articles/new", name="create_article")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(ArticleType::class, null, [
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Article $article */
            $article = $form->getData();
            $article->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();

            foreach ($article->getTags() as $tag) {
                $persistedTag = $em->getRepository('AppBundle:Tag')->findOneBy(['name' => $tag->getName()]);
                if (null !== $persistedTag) {
                    $article->removeTag($tag);
                    $article->addTag($persistedTag);
                }
            }
            $em->persist($article);
            $em->flush();

            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->addListenerService('kernel.terminate', ['notification.mail.subscriber', 'onTerminate']);

          return $this->redirectToRoute('app_articles_index');
        }

        return $this->render('AppBundle:Articles:new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/")
     * @Method({"GET","HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $articles = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->getLastArticles();

        $paginator = $this->get('knp_paginator');
        $paginateArticle = $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('@App/Articles/index.html.twig', ['articles' => $paginateArticle]);
    }

    /**
     * @Route("/articles/{slug}", name="show_article")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articleShow(Request $request, Article $article)
    {
        if (!$article->getIsModerated()) {
            throw new AccessDeniedHttpException('This Article is not moderated!');
        }

        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if (!$article->getViewUsers()->contains($user) and $user) {
            $eventDispatcher = $this->get('event_dispatcher');
            $event = new MyCustomEvent($user, $article);
            $eventDispatcher->dispatch(MyCustomEvent::NAME, $event);
        }

        $views = $this->get('tchoulom.view_counter')->getViews($article);
        $viewCounter = $this->get('tchoulom.view_counter')->getViewCounter($article);
        $viewCounter = (null != $viewCounter ? $viewCounter : new ViewCounter());

        if ($this->get('tchoulom.view_counter')->isNewView($viewCounter)) {
            $viewCounter->setIp($request->getClientIp());
            $viewCounter->setArticle($article);
            $viewCounter->setViewDate(new \DateTime('now'));

            $article->setViews($views);

            $em->persist($viewCounter);
            $em->persist($article);
            $em->flush();
        }

        $commentsCount = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->getAllModeratedCommentsFromArticle($article);

        $commentForm = $this->createForm(CommentType::class);

        $viewsCount = $article->getViewUsers()->count();

        $countPage = $views + $viewsCount;
        $contains = $article->getLikeUsers()->contains($this->getUser());

        return $this->render('@App/Articles/article_show.html.twig', [
            'article' => $article,
            'commentForm' => $commentForm->createView(),
            'contains' => $contains,
            'viewsCount' => $countPage,
            'commentsCount' => $commentsCount
        ]);
    }

    /**
     * @Route("/articles/{slug}/edit")
     * @Method({"GET","HEAD", "PUT"})
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response*
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, Article $article)
    {
        /** @var User $author */
        $author = $article->getUser();
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getId() !== $author->getId()) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm(ArticleType::class, $article, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $tags = $article->getTags()->toArray();
            $article->getTags()->clear();
            $article->setIsModerated(false);

            /** @var Tag $tag */
            foreach ($tags as $tag) {
                $persistedTag = $em->getRepository('AppBundle:Tag')->findOneBy(['name' => $tag->getName()]);

                if ($persistedTag and $persistedTag->getName() === $tag->getName()) {
                    $article->removeTag($tag);
                    $article->addTag($persistedTag);
                    $persistedTag->addArticle($article);
                } elseif (!$persistedTag) {
                    $article->addTag($tag);
                }
            }

            $em->flush($article);

            return $this->redirectToRoute("app_articles_index");
        }

        return $this->render('AppBundle:Articles:edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/article/{id}/remove", requirements={"id": "\d+"})
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response*
     * @Security("has_role('ROLE_USER')")
     */
    public function removeAction(Article $article)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var User $author */
        $author = $article->getUser();

        if ($user->getId() == $author->getId()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute("app_articles_index");
    }
}