<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


class RestUserController extends FOSRestController
{
    /**
     * @Annotations\View(serializerGroups={"user"})
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a collection of User",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   },
     *     output={"collection"=true, "collectionName"="users", "class"="AppBundle\Entity\User"}
     * )
     * @param Request $request
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getUsersAction(Request $request)
    {
        return $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getRepository('AppBundle:User')->findAll(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            $request->query->getInt('limit', 10)/*limit per page*/
        );
    }

    /**
     * @param User $user
     * @return User
     * @Annotations\View(serializerGroups={"user"})
     * @ApiDoc(
     *   resource = true,
     *   description = "Return an user identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   },
     *      parameters={
     *      {"name"="user", "dataType"="integer", "required"=true, "description"="user id"}
     *  },
     *     output={"collection"=true, "collectionName"="users", "class"="AppBundle\Entity\User"}
     * )
     */
    public function getUserAction(User $user)
    {
        return $user;
    }

    /**
     * @param User $user
     * @return Article[]
     * @Annotations\View()
     * @ApiDoc(
     *   resource = true,
     *   description = "Return all articles of an user identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   },
     *      parameters={
     *      {"name"="user", "dataType"="integer", "required"=true, "description"="user id"}
     *  },
     *     output={"collection"=true, "collectionName"="articles", "class"="AppBundle\Entity\Article"}
     * )
     */
    public function getUserArticlesAction(User $user)
    {
        return $user->getArticles()->toArray();
    }

    /**
     * @param User $user
     * @return Comment[]
     * @Annotations\View(serializerGroups={"user", "comment"})
     * @ApiDoc(
     *   resource = true,
     *   description = "Return all comments of an user identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   },
     *      parameters={
     *      {"name"="user", "dataType"="integer", "required"=true, "description"="user id"}
     *  },
     *     output={"collection"=true, "collectionName"="comments", "class"="AppBundle\Entity\Comment"}
     * )
     */
    public function getUserCommentsAction(User $user)
    {
        return $user->getComments()->toArray();
    }
}
