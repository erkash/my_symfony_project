<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use AppBundle\Form\CommentType;
use AppBundle\Form\FormErrorsTrait;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RestCommentController extends FOSRestController
{
    use FormErrorsTrait;

    /**
     * @Annotations\View()
     * @ApiDoc(
     *   resource = true,
     *   description = "Returns a collection of Comment",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the comment is not found"
     *   },
     *     output={"collection"=true, "collectionName"="comments", "class"="AppBundle\Entity\Comment"}
     * )
     * @param Request $request
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getCommentsAction(Request $request)
    {
        return $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getRepository('AppBundle:Comment')->findAll(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            $request->query->getInt('limit', 10)/*limit per page*/
        );
    }

    /**
     * @param Comment $comment
     * @return Comment
     * @Annotations\View()
     * @ApiDoc(
     *   resource = true,
     *   description = "Return a comment identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the comment is not found"
     *   },
     *      parameters={
     *      {"name"="comment", "dataType"="integer", "required"=true, "description"="comment id"}
     *  },
     *     output={"collection"=true, "collectionName"="comments", "class"="AppBundle\Entity\Comment"}
     * )
     */
    public function getCommentAction(Comment $comment)
    {
        return $comment;
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return Response|view
     * @ApiDoc(
     *     description = "Creates new comment",
     *     input="AppBundle\Form\CommentType",
     *     output="AppBundle\Entity\Comment",
     *     statusCodes = {
     *          201 = "Returned when successful",
     *          404 = "Returned when not found"
     *   }
     *  )
     */
    public function postCommentAction(Request $request, Article $article)
    {
        if (!$article) {
            throw new NotFoundHttpException('Article not found');
        }
        $form = $this->createForm(CommentType::class, null, [
            'csrf_protection' => false
        ]);

        $form->handleRequest($request);

        /** @var Comment $comment */
        $comment = $form->getData();
        $errors = $this->get('validator')->validate($comment, null, ['Comment_create']);

        if ($errors->count() == 0) {
            $comment->setUser($this->getUser());
            $comment->setArticle($article);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $view = $this->View($comment, 200);
            return $this->handleView($view);
        } else {
            $view = $this->getErrorsView($errors);
            return $view;
        }

    }

    /**
     * Delete a comment identified by id.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete a comment identified by id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the comment is not found"
     *   }
     * )
     *
     * @param Comment $comment
     * @return View
     */
    public function deleteCommentAction(Comment $comment)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var User $author */
        $author = $comment->getUser();

        if ($user->getId() == $author->getId()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();

            $view = View::create();
            $view->setStatusCode(204);

            return $view;
        } else {
            throw new AccessDeniedHttpException('Access denied');
        }
    }
}