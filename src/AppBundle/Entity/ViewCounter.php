<?php

namespace AppBundle\Entity;
use DateTime;
use Tchoulom\ViewCounterBundle\Entity\ViewCounter as BaseViewCounter;
use Doctrine\ORM\Mapping as ORM;


/**
 * ViewCounter.
 *
 * @ORM\Table(name="view_counter")
 * @ORM\Entity()
 */
class ViewCounter extends BaseViewCounter
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="Article", cascade={"persist"}, inversedBy="viewCounters")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $article;
    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string")
     */
    protected $ip;

    /**
     * @var datetime $viewDate
     *
     * @ORM\Column(type="datetime")
     */
    protected $viewDate;

    /**
     * Gets article
     *
     * @return Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Sets Article
     *
     * @param Article $article
     *
     * @return $this
     */
    public function setArticle(Article $article)
    {
        $this->article = $article;

        return $this;
    }
}
