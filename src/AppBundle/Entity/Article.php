<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Tchoulom\ViewCounterBundle\Model\ViewCountable;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;


/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 * @ORM\HasLifecycleCallbacks
 * @ExclusionPolicy("all")
 */
class Article implements ViewCountable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     * @Assert\Length(
     *      min = 10,
     *      minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *      groups={"Article_create"}
     * )
     * @Expose()
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ViewCounter", mappedBy="article", orphanRemoval=true, cascade={"persist"})
     */
    protected $viewCounters;

    /**
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    protected $views = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\Length(
     *      min = 100,
     *      minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *      groups={"Article_create"}
     * )
     * @Expose()
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Length(
     *      min = 50,
     *      minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *      groups={"Article_create"}
     * )
     * @Expose()
     */
    private $summary;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"})
     *
     * @ORM\Column(type="string")
     * @Expose()
     */
    private $slug;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     * @Expose()
     */
    protected $createdAt;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime", nullable = true)
     * @Expose()
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="articles")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="likeArticles")
     * @ORM\JoinTable(
     *      name="likes",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     **/
    private $likeUsers;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="viewArticles")
     * @ORM\JoinTable(
     *      name="views",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     **/
    private $viewUsers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="article", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"publishedAt": "DESC"})
     */
    private $comments;

    /**
     * @var Tag[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag", inversedBy="articles", cascade={"persist"})
     * @ORM\JoinTable(name="articles_tags")
     * @ORM\OrderBy({"name": "ASC"})
     */
    private $tags;

    /**
     * @ORM\Column(name="comments_is_disabled", type="boolean")
     */
    private $commentsIsDisabled;

    /**
     * @ORM\Column(name="is_moderated", type="boolean")
     */
    private $isModerated;

    public function __toString()
    {
        return $this->title ? : '';
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Article
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Article
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments     = new ArrayCollection();
        $this->tags         = new ArrayCollection();
        $this->likeUsers    = new ArrayCollection();
        $this->viewUsers    = new ArrayCollection();
        $this->viewCounters = new ArrayCollection();
        $this->isModerated  = 0;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Article
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Article
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getCommentsIsDisabled()
    {
        return $this->commentsIsDisabled;
    }

    /**
     * @param $commentsIsDisabled
     */
    public function setCommentsIsDisabled($commentsIsDisabled)
    {
        $this->commentsIsDisabled = $commentsIsDisabled;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return Article
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(Tag $tag)
    {

        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add likeUser
     *
     * @param \AppBundle\Entity\User $likeUser
     *
     * @return Article
     */
    public function addLikeUser(User $likeUser)
    {
        $this->likeUsers[] = $likeUser;

        return $this;
    }

    /**
     * Remove likeUser
     *
     * @param \AppBundle\Entity\User $likeUser
     */
    public function removeLikeUser(User $likeUser)
    {
        $this->likeUsers->removeElement($likeUser);
    }

    /**
     * Get likeUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikeUsers()
    {
        return $this->likeUsers;
    }

    /**
     * Get viewUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewUsers()
    {
        return $this->viewUsers;
    }

    /**
     * @param User $viewUser
     */
    public function addViewUser(User $viewUser)
    {
        $this->viewUsers[] = $viewUser;
    }

    /**
     * Remove viewUser
     *
     * @param User $viewUser
     */
    public function removeViewUser(User $viewUser)
    {
        $this->viewUsers->removeElement($viewUser);
    }

    /**
     * Sets $views
     *
     * @param integer $views
     *
     * @return $this
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Gets $views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Get $viewCounters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewCounters()
    {
        return $this->viewCounters;
    }

    /**
     * Add $viewCounter
     *
     * @param ViewCounter $viewCounter
     *
     * @return $this
     */
    public function addViewCounter(ViewCounter $viewCounter)
    {
        $this->viewCounters[] = $viewCounter;

        return $this;
    }

    /**
     * Remove $viewCounter
     *
     * @param ViewCounter $viewCounter
     */
    public function removeViewCounter(ViewCounter $viewCounter)
    {
        $this->comments->removeElement($viewCounter);
    }

    /**
     * @return boolean
     */
    public function getIsModerated()
    {
        return $this->isModerated;
    }

    /**
     * @param boolean $isModerated
     *
     * @return Article
     */
    public function setIsModerated($isModerated)
    {
        $this->isModerated = $isModerated;

        return $this;
    }
}
