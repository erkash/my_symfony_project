<?php

namespace AppBundle\Event;


use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class MyCustomEvent extends Event
{
    const NAME = 'my.custom.event';

    /**
     * @var User
     */
    private $user;
    /**
     * @var Article
     */
    private $article;

    public function __construct(User $user, Article $article)
    {
        $this->user    = $user;
        $this->article = $article;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }
}