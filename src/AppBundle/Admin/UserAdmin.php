<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            ->add('password')
            ->add('lastLogin')
            ->add('confirmationToken')
            ->add('passwordRequestedAt')
            ->add('roles')
            ->add('facebookId')
            ->add('facebookAccessToken')
            ->add('googleId')
            ->add('googleAccessToken')
            ->add('name')
            ->add('surname')
            ->add('avatarIsChanged')
            ->add('avatar');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('roles')
            ->add('name')
            ->add('surname')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('plainPassword', 'repeated', [
                'type' => 'password',
                'options' => ['translation_domain' => 'FOSUserBundle'],
                'first_options' => ['label' => 'form.password'],
                'second_options' => ['label' => 'form.password_confirmation'],
                'invalid_message' => 'fos_user.password.mismatch',
            ])
            ->add('roles')
            ->add('name')
            ->add('surname');
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('password')
            ->add('lastLogin')
            ->add('roles')
            ->add('facebookId')
            ->add('facebookAccessToken')
            ->add('googleId')
            ->add('googleAccessToken')
            ->add('name')
            ->add('surname')
            ->add('followers', null, [
                'route' => ['name' => 'show']
            ])
            ->add('avatarIsChanged')
            ->add('avatar', null, ['template' => 'AppBundle:Admin:show_image.html.twig']);
    }

    public function getFormBuilder()
    {
        if (is_null($this->getSubject()->getId())) {
            $this->formOptions = ['validation_groups' => 'Registration'];
        }

        $formBuilder = parent::getFormBuilder();
        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }
}
