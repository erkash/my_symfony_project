<?php

namespace AppBundle\EventSubscriber;


use AppBundle\Event\MyCustomEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ViewCounterSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onViewArticle(MyCustomEvent $event)
    {
        $user = $event->getUser();
        $article = $event->getArticle();
        $user->addViewArticle($article);
        $article->addViewUser($user);

        $this->em->persist($article);
        $this->em->persist($user);
        $this->em->flush();
    }

    public static function getSubscribedEvents()
    {
        return ['kernel.view' => 'onViewArticle'];
    }
}