<?php

namespace AppBundle\EventSubscriber;


use AppBundle\Entity\Article;
use AppBundle\Entity\User;
use AppBundle\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MailNotificationSubscriber
{
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;


    public function __construct(Mailer $mailer, EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->mailer        = $mailer;
        $this->entityManager = $entityManager;
        $this->tokenStorage  = $tokenStorage;
    }

    public function onTerminate()
    {
        /** @var User $author */
        $author = $this->tokenStorage->getToken()->getUser();
        /** @var Article $article */
        $article = $this->entityManager->getRepository('AppBundle:Article')->getLastCreatedArticleByUser($author);

        $this->mailer->create($article, $author)->send();
    }

}