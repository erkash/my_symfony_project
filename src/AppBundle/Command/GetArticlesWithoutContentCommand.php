<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class GetArticlesWithoutContentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:get-articles-csv')
            // the short description shown while running "php bin/console list"
            ->setDescription('Get Articles.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to get articles...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $articles = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Article')->findAll();
        $data = [];

        foreach ($articles as $article) {
            $data[] = [
                'id' => $article->getId(),
                'slug' => $article->getSlug(),
                'title' => $article->getTitle(),
                'summary' => $article->getSummary()
            ] ;
        }

        $fp = fopen('file.csv', 'w');

        fputcsv($fp, array_keys($data['0']));

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);

        $output->writeln([
            'Get Articles',
            '============',
            '',
        ]);

        // outputs a message followed by a "\n"
        $output->writeln('Whoa!');

        // outputs a message without adding a "\n" at the end of the line
        $output->write('You are about to ');
        $output->write('create a user.' . "\n");
    }

}